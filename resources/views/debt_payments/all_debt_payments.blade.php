@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <div class="row">
              <div class="col-md-10">
                <h2>Debt Payments List For {{$debt_payment_title}}</h2>
              </div>
              <div class="col-md-2">
                <a class="btn btn-success float-right" href="{{ route('debt_payments.create') }}"> Add Payment</a>
              </div>

            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of debt payments for {{$debt_payment_title}} available in the database
            </p>

            <h5 class="text-success">Total Debt Payments for {{$debt_payment_title}} is KES: {{$debt_payment_amount}}</h5>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Customer Name</th>
                  <th>Phone Number</th>
                  <th>Description</th>
                  <th>Amount Paid</th>
                  <th>Added On</th>
                  <th>Added By</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($debt_payments as $debt_payment)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$debt_payment->customer->name}}</td>
                    <td>{{$debt_payment->phone_number}}</td>
                    <td>{{$debt_payment->description}}</td>
                    <td>{{$debt_payment->amount_paid}}</td>
                    <td>{{$debt_payment->created_at}}</td>
                    @if ($debt_payment->user != '')
                    <td>{{$debt_payment->user->name}}</td>
                    @else
                    <td>UNKNOWN</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-info" href="debt_payments/{{$debt_payment->id}}">Manage</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
