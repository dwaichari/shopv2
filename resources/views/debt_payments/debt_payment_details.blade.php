@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Debt Payment Details</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="col-md-12 col-sm-12 col-xs-12" style="border:0px solid #e5e5e5;">

              <h3 class="prod_title" >Customer Name: {{$debt_payment->customer->name}}</h3>

              <p>Description: {{$debt_payment->description}}</p>
              <h2>Amount Paid: {{$debt_payment->amount_paid}}</h2>
              <br>
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                          <form action="" method="post" action="{{route('debt_payments.destroy',$debt_payment->id)}}">
                            @csrf
                            @method('delete')
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Delete {{$debt_payment->name}}</h4>
                        </div>
                        <div class="modal-body">
                          <p>Are you sure you want to delete {{$debt_payment->name}}? Remember if you delete this debt payment, The amount will be restored as debt</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" >Yes Delete</button>
                        </div>
                    </form>
                      </div>
                    </div>
                  </div>
                {{-- <a type="button" class="btn btn-warning" href="/debt_payments/{{$debt_payment->id}}/edit">Edit</a> --}}
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm">Delete</button>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
