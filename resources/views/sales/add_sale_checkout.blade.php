@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Sale Checkout</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="text-center text-danger">{{$error}}</div>
                @endforeach
            @endif
            <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="col-md-8 col-sm-8">
                    <form id="demo-form2"  class="form-horizontal form-label-left"  action="/sales_checkout" method="post" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <input type="hidden" name="sale_id"  id="sale_id"  class="form-control col-md-7 col-xs-12" value="{{$sale->id}}">
                        <input type="hidden"  id="initial_balance"  class="form-control col-md-7 col-xs-12" value="{{$sale->balance}}">
                        <input type="hidden"  id="grandtotal"  class="form-control col-md-7 col-xs-12" value="{{$sale->total_selling_price}}">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_id">Product <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" id="product_id" name="product_id" required>
                                <option value="">Choose product</option>
                                @foreach ($products as $product)
                                <option value="{{$product->id}}">{{$product->name}}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit_buying_price">Unit Buying Price <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="unit_buying_price"  id="unit_buying_price" required="required" class="form-control col-md-7 col-xs-12" readonly>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit_selling_price">Unit Selling Price <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="unit_selling_price"  id="unit_selling_price" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_of_items">No of Items <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="no_of_items"  id="no_of_items"  class="form-control col-md-7 col-xs-12" required>
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="customer_name">Customer Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" name="customer_name"  id="customer_name"  class="form-control col-md-7 col-xs-12" value="{{$sale->customer_name}}">
                            </div>
                          </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-primary">Add Item</button>
                          </div>
                        </div>
                      </form>
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="" alt="" id="product_image" style=" max-height:100%; max-width:100%;">

                    <h5 class="price" id="in_stock"></h5>
                </div>
            </div>
        </div>
        <div class="row" style="background-color: rgb(200, 243, 250); margin-bottom:5px">
            <form action="/sales_confirm" method="POST" autocomplete="off">
                @csrf
            <div class="col-md-3 col-sm-3">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_selling_price">Grand Total<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2 class="price"> KES {{$sale->total_selling_price}}</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cash_received">Cash Received<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="number" name="cash_received"  id="cash_received"  class="form-control col-md-7 col-xs-12" required value="{{$sale->cash_received}}" step="0.01">
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="balance">Balance<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="number" name="balance"  id="balance"  class="form-control col-md-7 col-xs-12" required value="{{$sale->balance}}" readonly>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <input type="hidden" name="sale_id"  class="form-control col-md-7 col-xs-12" value="{{$sale->id}}">
                          <button type="submit" class="btn btn-warning">Confirm Transaction</button>
                        </div>
                      </div>
                </div>
                <div class="row text-center">
                    <h5 class="text-center">Grand Profit is Kes:{{$sale->total_profit}}</h5>
                </div>
            </div>
        </form>
        </div>
        <div class="row">
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
                <thead>
                  <tr>
                    <th class="text-center">S/N</th>
                    <th>Product</th>
                    <th>Unit Buying Price</th>
                    <th>Unit Selling Price</th>
                    <th>No of Items</th>
                    <th>Total</th>
                    <th>Profit</th>
                    <th>Added On</th>
                    <th>Manage</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($sale_items as $itemsale)
                    <tr>
                      <td class="text-center">{{$loop->index + 1}}</td>
                      <td>{{$itemsale->product->name}}</td>
                      <td>{{$itemsale->unit_buying_price}}</td>
                      <td>{{$itemsale->unit_selling_price}}</td>
                      <td>{{$itemsale->no_of_items}}</td>
                      <td>{{$itemsale->total_selling_price}}</td>
                      <td>{{$itemsale->profit}}</td>
                      <td>{{$itemsale->created_at}}</td>
                      <td>
                          <a class="btn btn-sm btn-info" href="/sale_item/remove/{{$itemsale->id}}">Delete</a>
                      </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="row">
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Delete sale</h4>
                        </div>
                        <div class="modal-body">
                          <p>Are you sure you want to delete this sale?</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <a type="submit" class="btn btn-primary" href="/delete_sales/{{$sale->id}}">Yes Delete</a>
                        </div>
                      </div>
                    </div>
                  </div>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm">Delete Sale</button>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection

@section('custom-scripts')
    {{-- custom jquery --}}
    <script>
 $(document).ready(function(){
            //retrive the product and check if there is an image when making a sale
            $('#product_id').on('change', function(){
                $.get('/api/product/'+$( "#product_id").val()+'/image_url',  // url
                    function (data, textStatus, jqXHR) {
                        //fill unit buying price
                        $('#unit_buying_price').val(data.unit_buying_price);
                        $('#unit_selling_price').val(data.unit_selling_price);
                        if (data.image_url !='') {
                            $("#product_image").attr("src",data.image_url);
                        }else{
                            $("#product_image").attr("src",'/admin/images/no-image.png');
                        }
                        //show amount in stock
                        $('#in_stock').html(data.in_stock+' In Stock');
                });
            });
            //update balance
            $('#cash_received').on('blur', function(){
                //update initial balance
                let initial_balance = parseFloat($('#grandtotal').val());
                let cash_recieved = parseFloat($('#cash_received').val());
                let balance = 0;
                //check if the balance was negative
                if (initial_balance < 0) {
                    balance = cash_recieved + initial_balance;
                } else {
                    balance = cash_recieved - initial_balance;
                }

                console.log(balance);
                //update balance
                $('#balance').val(balance);
            })
        });
     </script>
@endsection
