@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Sales List</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of sale added
            </p>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Sold At</th>
                  <th>Items Sold</th>
                  <th>Total Buying Price</th>
                  <th>Total Selling Price</th>
                  <th>Cash Received</th>
                  <th>Balance</th>
                  <th>Total Profit</th>
                  <th>Status</th>
                  <th>Customer Name</th>
                  <th>Added By</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($sales as $sale)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$sale->created_at}}</td>
                    <td>
                            @foreach ($sale->itemsales as $item)
                            <span>{{'->'.$item->no_of_items.' '.$item->product->name.' @'.$item->unit_selling_price.' |Khs '.$item->unit_selling_price*$item->no_of_items}}</span> <br>
                    @endforeach
                </td>
                    <td>{{$sale->total_buying_price}}</td>
                    <td>{{$sale->total_selling_price}}</td>
                    <td>{{$sale->cash_received}}</td>
                    <td>{{$sale->balance}}</td>
                    <td>{{$sale->total_profit}}</td>
                    <td>{{$sale->status}}</td>
                    <td>{{$sale->customer_name}}</td>
                    @if ($sale->user != '')
                    <td>{{$sale->user->name}}</td>
                    @else
                    <td>UNKNOWN</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-info" href="sales_checkout/{{$sale->id}}">Details</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
