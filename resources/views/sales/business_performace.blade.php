@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <div class="col-md-6">
                    <h4>Sales List [{{$productStringText}}]: <small>{{$dateRangeString}}</small></h4>
                    <h4 class="text-success">Projected Profit is <span class="text-primary">KES {{$projected_profit}}</span></h4>
                </div>
                <form action="business_performance" method="get">
                  <div class="col-md-2">
                    <select class="form-control" id="product_id" name="product_id">
                      <option value="">Choose product</option>
                      @foreach ($products as $product)
                      <option value="{{$product->id}}">{{$product->name}}</option>
                      @endforeach
                    </select>
                  </div>
                <div class="col-md-2">
                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                    <span id="dateRange">{{$dateStringText}}</span> <b class="caret"></b>
                    <input type="hidden" id="dateRangeText" name="dateRangeText">
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-success btn-sm">Filter</button>
                </div>
            </form>
          </div>
          <div class="x_content">
            <div class="row">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-money"></i>
                    </div>
                    <div class="count">{{$stock_value}}</div>

                    <h3>Stock Value [{{$productStringText}}]</h3>
                  </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-caret-square-o-right"></i>
                    </div>
                    <div class="count">{{$sales->sum('total_selling_price')}}</div>

                    <h3>Sales</h3>
                  </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-comments-o"></i>
                    </div>
                    <div class="count">{{$sales->sum('total_profit')}}</div>

                    <h3>Profit</h3>
                  </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-gear"></i>
                    </div>
                    <div class="count">{{$projected_sales}}</div>

                    <h3>Projected Sales [{{$productStringText}}]</h3>
                  </div>
                </div>
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Added On</th>
                  <th>Items Sold</th>
                  <th>Total Buying Price</th>
                  <th>Total Selling Price</th>
                  <th>Cash Received</th>
                  <th>Balance</th>
                  <th>Total Profit</th>
                  <th>Status</th>
                  <th>Customer Name</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($sales as $sale)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$sale->created_at}}</td>
                    <td>
                            @foreach ($sale->itemsales as $item)
                            <span>{{'->'.$item->no_of_items.' '.$item->product->name.' @'.$item->unit_selling_price.' |Khs '.$item->unit_selling_price*$item->no_of_items}}</span> <br>
                    @endforeach
                </td>
                    <td>{{$sale->total_buying_price}}</td>
                    <td>{{$sale->total_selling_price}}</td>
                    <td>{{$sale->cash_received}}</td>
                    <td>{{$sale->balance}}</td>
                    <td>{{$sale->total_profit}}</td>
                    <td>{{$sale->status}}</td>
                    <td>{{$sale->customer_name}}</td>
                    <td>
                        <a class="btn btn-sm btn-info" href="sales_checkout/{{$sale->id}}">Details</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="dashboard_graph">

      <div class="row x_title">
          <h3>Business Perfomance [{{$productStringText}}]<small>Last 12 months [sales vs profits vs noOfItems]</small></h3>
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_content"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; inset: 0px;"></iframe>
              <canvas id="businessChart" width="511" height="255" style="width: 511px; height: 255px;"></canvas>
          </div>
      </div>
      <div class="clearfix"></div>
  </div>
  </div>

</div>
@endsection

@section('custom-scripts')
<script>
$(document).ready(function(){

    $("#dateRange").on('DOMSubtreeModified',function(){
        $('#dateRangeText').val($("#dateRange").text());
    });

    //before getting api data
  var urlParams = new URLSearchParams(window.location.search);
  if(urlParams.has('product_id')){
    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    var product_id = getUrlVars()["product_id"];

    $.get('api/get_chart_data/'+product_id, function(data, textStatus, jqXHR){
        console.log(data)
        var ctx = document.getElementById("businessChart");
    var businessChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: data['months'],
        datasets: [
        {
        label: "Sales",
        backgroundColor: "rgba(38, 185, 154, 0.31)",
        borderColor: "rgba(38, 185, 154, 0.7)",
        pointBorderColor: "rgba(38, 185, 154, 0.7)",
        pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointBorderWidth: 1,
        data: data['sales']
        }, 
        {
        label: "Items Sold",
        backgroundColor: "rgba(0, 0, 255, 0.31)",
        borderColor: "rgba(255, 0, 0, 0.7)",
        pointBorderColor: "rgba(38, 185, 154, 0.7)",
        pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointBorderWidth: 1,
        data: data['items_sold']
        }, 
        {
        label: "Profits",
        backgroundColor: "rgba(3, 88, 106, 0.3)",
        borderColor: "rgba(3, 88, 106, 0.70)",
        pointBorderColor: "rgba(3, 88, 106, 0.70)",
        pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(151,187,205,1)",
        pointBorderWidth: 1,
        data: data['profits']
        }]
    },
    });
    })
  } else{
    $.get('api/get_chart_data', function(data, textStatus, jqXHR){
        console.log(data)
        var ctx = document.getElementById("businessChart");
    var businessChart = new Chart(ctx, {
    type: 'line',

    data: {
        labels: data['months'],
        datasets: [
        {
        label: "Sales",
        backgroundColor: "rgba(38, 185, 154, 0.31)",
        borderColor: "rgba(38, 185, 154, 0.7)",
        pointBorderColor: "rgba(38, 185, 154, 0.7)",
        pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointBorderWidth: 1,
        data: data['sales']
        }, 
        {
        label: "Items Sold",
        backgroundColor: "rgba(0, 0, 255, 0.31)",
        borderColor: "rgba(255, 0, 0, 0.7)",
        pointBorderColor: "rgba(38, 185, 154, 0.7)",
        pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointBorderWidth: 1,
        data: data['items_sold']
        }, 
        {
        label: "Profits",
        backgroundColor: "rgba(3, 88, 106, 0.3)",
        borderColor: "rgba(3, 88, 106, 0.70)",
        pointBorderColor: "rgba(3, 88, 106, 0.70)",
        pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(151,187,205,1)",
        pointBorderWidth: 1,
        data: data['profits']
        }]
    },
    });
    })
  }// true
})
</script>
@endsection
