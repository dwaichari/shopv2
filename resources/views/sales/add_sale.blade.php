@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Add a New Sale</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="text-center text-danger">{{$error}}</div>
                @endforeach
            @endif
            <div class="col-sm-12 col-md-12">
                <div class="col-md-8 col-sm-8">
                    <form id="demo-form2"  class="form-horizontal form-label-left"  action="/sales_checkout" method="post" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_id">Product <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" id="product_id" name="product_id" required>
                                <option value="">Choose product</option>
                                @foreach ($products as $product)
                                <option value="{{$product->id}}">{{$product->name}}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit_buying_price">Unit Buying Price <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="unit_buying_price"  id="unit_buying_price"  class="form-control col-md-7 col-xs-12" required readonly>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit_selling_price">Unit Selling Price <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="unit_selling_price"  id="unit_selling_price" required="required" class="form-control col-md-7 col-xs-12" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_of_items">No of Items <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="no_of_items"  id="no_of_items" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="customer_name">Customer Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" name="customer_name"  id="customer_name"  class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>
                      </form>
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="" alt="" id="product_image" style=" max-height:100%; max-width:100%;">
                    <h5 class="price" id="in_stock"></h5>
                </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection

@section('custom-scripts')
    {{-- custom jquery --}}
    <script>
 $(document).ready(function(){
            //retrive the product and check if there is an image when making a sale
            $('#product_id').on('change', function(){
                $.get('/api/product/'+$( "#product_id").val()+'/image_url',  // url
                    function (data, textStatus, jqXHR) {
                        //fill unit buying price
                        $('#unit_buying_price').val(data.unit_buying_price);
                        $('#unit_selling_price').val(data.unit_selling_price);
                        if (data.image_url !='') {
                            $("#product_image").attr("src",data.image_url);
                        }else{
                            $("#product_image").attr("src",'/admin/images/no-image.png');
                        }

                        //show amount in stock
                        $('#in_stock').html(data.in_stock+' In Stock');
                });
            })
        });
     </script>
@endsection
