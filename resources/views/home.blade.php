@extends('layouts.master')

@section('main-content')
<!-- top tiles -->
<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
    <div class="count">{{$users->count()}}</div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-clock-o"></i> Total Products</span>
    <div class="count">{{$products->count()}}</div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Sales Today</span>
    <div class="count green">{{$sales_today->sum('total_selling_price')}}</div>
    </div>
    @can('can-view-business-report')
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Profit Today</span>
    <div class="count">{{$sales_today->sum('total_profit')}}</div>
    </div>
    @endcan
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Fuel Pumps</span>
    <div class="count">{{$fuel_pumps->count()}}</div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Items In Stock</span>
    <div class="count">{{$products->sum('in_stock')}}</div>
    </div>
</div>
<!-- /top tiles -->
@can('can-view-business-report')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="dashboard_graph">

        <div class="row x_title">
            <h3>Business Perfomance <small>Last One Year [sales vs profits]</small></h3>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_content"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; inset: 0px;"></iframe>
                <canvas id="businessChart" width="511" height="255" style="width: 511px; height: 255px;"></canvas>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    </div>
</div>
@endcan
<br />
@endsection
@section('custom-scripts')
<script>
    $.get('api/get_chart_data', function(data, textStatus, jqXHR){
        console.log(data)
        var ctx = document.getElementById("businessChart");
    var businessChart = new Chart(ctx, {
    type: 'line',

    data: {
        labels: data['months'],
        datasets: [{
        label: "Sales",
        backgroundColor: "rgba(38, 185, 154, 0.31)",
        borderColor: "rgba(38, 185, 154, 0.7)",
        pointBorderColor: "rgba(38, 185, 154, 0.7)",
        pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointBorderWidth: 1,
        data: data['sales']
        }, {
        label: "Profits",
        backgroundColor: "rgba(3, 88, 106, 0.3)",
        borderColor: "rgba(3, 88, 106, 0.70)",
        pointBorderColor: "rgba(3, 88, 106, 0.70)",
        pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(151,187,205,1)",
        pointBorderWidth: 1,
        data: data['profits']
        }]
    },
    });
    })
</script>

@endsection
