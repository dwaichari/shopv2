@extends('layouts.master')
@section('main-content')
<div class="col-md-12">
    <div class="col-middle">
      <div class="text-center text-center">
        <h1 class="error-number">403</h1>
        <h2>Access denied</h2>
        <p>Full authentication is required to access this resource.</p>
        <p>Please contact your administrator.</p>
        <button  class="btn btn-round btn-info" id="goBack">Go Back</button>
      </div>
    </div>
  </div>
@endsection

@section('custom-scripts')
<script>
 $(document).ready(function(){
         $("#goBack").on('click', function() {
            history.back();
         }) 
    });
</script>
@endsection