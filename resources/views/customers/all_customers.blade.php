@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Customers List</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of customers available in the database
            </p>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Name</th>
                  <th>Phone Number</th>
                  <th>Description</th>
                  <th>Debt Amount</th>
                  <th>Added On</th>
                  <th>Added By</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($customers as $customer)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$customer->name}}</td>
                    <td>{{$customer->phone_number}}</td>
                    <td>{{$customer->description}}</td>
                    <td>{{$customer->debt_amount}}</td>
                    <td>{{$customer->created_at}}</td>
                    @if ($customer->user != '')
                    <td>{{$customer->user->name}}</td>
                    @else
                    <td>UNKNOWN</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-info" href="customers/{{$customer->id}}">Manage</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
