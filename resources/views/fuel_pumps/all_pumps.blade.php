@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Fuel Pumps List</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of Pumps Added
            </p>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Pump Name</th>
                  <th>Previous Readings</th>
                  <th>Current Readings</th>
                  <th>Fuel Name</th>
                  <th>Added On</th>
                  <th>Added By</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($pumps as $pump)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$pump->pump_name}}</td>
                    @if ($pump['current_reading'] != null)
                    <td>{{$pump['current_reading']->previous_readings}}</td>
                    <td>{{$pump['current_reading']->current_readings}}</td>
                    @else
                    <td></td>
                    <td></td>   
                    @endif
                    <td>{{$pump->product->name}}</td>
                    <td>{{$pump->created_at}}</td>
                    @if ($pump->user != '')
                    <td>{{$pump->user->name}}</td>
                    @else
                    <td>UNKNOWN</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-warning" href="fuel_pumps/{{$pump->id}}">Edit</a>
                        <a class="btn btn-sm btn-danger" href="fuel_pumps/{{$pump->id}}">Delete</a>
                        <a class="btn btn-sm btn-primary" href="fuel_pump/{{$pump->id}}/calibrate">Calibrate</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
