@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Calibrate Pump Readings</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="text-center text-danger">{{$error}}</div>
                @endforeach
            @endif
            <div class="col-sm-12 col-md-12">
                <div class="col-md-8 col-sm-8">
                    <form id="demo-form2"  class="form-horizontal form-label-left"  action="/fuel_pump/{{$pump->id}}/calibrate" method="post" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pump_name">Pump Name<span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="pump_name" id="pump_name" class="form-control col-md-7 col-xs-12" readonly value="{{$pump->pump_name}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_id">Product<span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="product_name"  class="form-control col-md-7 col-xs-12" readonly value="{{$pump->product->name}}">
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="selling_price_per_litre">Previous Readings<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text"  class="form-control col-md-7 col-xs-12" id="previous_readings" readonly value="{{$pump['last_reading']->previous_readings}}">
                            </div>
                          </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="selling_price_per_litre">Selling Price Per Litre<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="number" name="selling_price_per_litre"  id="selling_price_per_litre"  class="form-control col-md-7 col-xs-12" step="0.01" min="0" value="{{$pump['last_reading']->selling_price_per_litre}}" readonly> 
                            </div>
                          </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="current_readings">Current Readings<span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="current_readings"  id="current_readings"  class="form-control col-md-7 col-xs-12" required step="0.01" min="0" value="{{$pump['last_reading']->current_readings}}" readonly>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="calibration_readings">Calibration Readings<span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="calibration_readings"  id="calibration_readings"  class="form-control col-md-7 col-xs-12" required step="0.01" min="0" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>

                      </form>
                </div>
                <div class="col-md-4 col-sm-4">
                    <h4>Pump's Product Image</h4>
                    @if($pump->image_url =='')
                    <img src="{{asset('admin/images/no-image.png')}}" alt="" id="product_image" style=" max-height:100%; max-width:100%;">
                    @else
                    <img src="{{$pump->image_url}}" alt="" id="product_image" style=" max-height:100%; max-width:100%;">
                    @endif
                    <h5 class="price" id="in_stock"></h5>
                </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection