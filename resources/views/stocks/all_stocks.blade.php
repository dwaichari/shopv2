@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Stock List</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of stock added
            </p>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Product Name</th>
                  <th>Supplier</th>
                  <th>Unit Buying Price</th>
                  <th>Unit Selling Price</th>
                  <th>No of Items</th>
                  <th>Amount</th>
                  <th>Added On</th>
                  <th>Added By</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($stocks as $stock)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$stock->product->name}}</td>
                    <td>{{$stock->suppliers_name}}</td>
                    <td>{{$stock->unit_buying_price}}</td>
                    <td>{{$stock->unit_selling_price}}</td>
                    <td>{{$stock->no_of_items_bought}}</td>
                    <td>{{$stock->total_amount}}</td>
                    <td>{{$stock->created_at}}</td>
                    @if ($stock->user != '')
                    <td>{{$stock->user->name}}</td>
                    @else
                    <td>UNKNOWN</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-info" href="stocks/{{$stock->id}}">Manage</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
