@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Edit Stock</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="text-center text-danger">{{$error}}</div>
                @endforeach
            @endif
            <div class="col-sm-12 col-md-12">
                <div class="col-md-8 col-sm-8">
                    <form id="demo-form2" class="form-horizontal form-label-left" action="/stocks/{{$stock->id}}" method="post" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_id">Product <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" name="name"   class="form-control col-md-7 col-xs-12" value="{{$stock->product->name}}" readonly>
                              <input type="hidden" name="product_id" id="product" class="form-control col-md-7 col-xs-12" value="{{$stock->product->id}}">
                            </div>
                          </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="suppliers_name">Supplier <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="suppliers_name"  id="suppliers_name"  class="form-control col-md-7 col-xs-12" value="{{$stock->suppliers_name}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_of_items_bought">No of Items <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="no_of_items_bought"  id="no_of_items_bought"  class="form-control col-md-7 col-xs-12" required value="{{$stock->no_of_items_bought}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_amount">Total Amount <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="total_amount"  id="total_amount" required="required" class="form-control col-md-7 col-xs-12" value="{{$stock->total_amount}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit_buying_price">Unit Buying Price <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="unit_buying_price"  id="unit_buying_price" required="required" class="form-control col-md-7 col-xs-12" readonly step="0.01" value="{{$stock->unit_buying_price}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit_selling_price">Unit Selling Price <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="unit_selling_price"  id="unit_selling_price" required="required" class="form-control col-md-7 col-xs-12" step="0.01" value="{{$stock->unit_selling_price}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Receipt </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" name="picture">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>

                      </form>
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="" alt="" id="product_image" style=" max-height:100%; max-width:100%;">
                    <h5 class="price" id="in_stock"></h5>
                </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection

@section('custom-scripts')
    {{-- custom jquery --}}
    <script>
 $(document).ready(function(){
            //retrive the product and check if there is an image when making a sale
                $.get('/api/product/'+$( "#product").val()+'/image_url',  // url
                    function (data, textStatus, jqXHR) {
                        //fill unit buying price
                        $('#unit_buying_price').val(data.unit_buying_price);
                        $('#unit_selling_price').val(data.unit_selling_price);
                        if (data.image_url !='') {
                            $("#product_image").attr("src",data.image_url);
                        }else{
                            $("#product_image").attr("src",'/admin/images/no-image.png');
                        }

                        //show amount in stock
                        $('#in_stock').html(data.in_stock+' In Stock');
                });
            //update the current unit buying price
            $('#total_amount').on('blur', function(){
                let total_amount = parseFloat($('#total_amount').val());
                let no_of_items = parseFloat($('#no_of_items_bought').val());
                $('#unit_buying_price').val(parseFloat(total_amount/no_of_items));
            })
        });
     </script>
@endsection
