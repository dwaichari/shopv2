@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <div class="row">
              <div class="col-md-10">
                <h2>Debts List History For {{$debt_title}}</h2>
              </div>
              <div class="col-md-2">
                <a class="btn btn-success float-right" href="{{ route('debts.create') }}"> Add Debt</a>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of debts history for {{$debt_title}} available in the database
            </p>

            <h5 class="text-success">Total Debt for {{$debt_title}} is KES: {{$debt_amount}}</h5>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Customer Name</th>
                  <th>Phone Number</th>
                  <th>Description</th>
                  <th>Debt Amount</th>
                  <th>Added On</th>
                  <th>Added By</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($debts as $debt)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$debt->customer->name}}</td>
                    <td>{{$debt->customer->phone_number}}</td>
                    <td>{{$debt->description}}</td>
                    <td>{{$debt->debt_amount}}</td>
                    <td>{{$debt->created_at}}</td>
                    @if ($debt->user != '')
                    <td>{{$debt->user->name}}</td>
                    @else
                    <td>UNKNOWN</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-info" href="debts/{{$debt->id}}">Manage</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
