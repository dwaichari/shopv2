@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Edit debt</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="text-center text-danger">{{$error}}</div>
                @endforeach
            @endif
            <form id="demo-form2"  class="form-horizontal form-label-left"  action="/debts/{{$debt->id}}" method="post" enctype="multipart/form-data" autocomplete="off">
                @csrf
                @method('PUT')
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="debt_name">Customer Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="customer_id"  id="debt_name" required="required" class="form-control col-md-7 col-xs-12" value="{{$debt->customer->name}}" readonly>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="description" name="description"  class="form-control" >{{$debt->description}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="debt_amount">Debt Amount <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="debt_amount"  id="debt_amount" required="required" class="form-control col-md-7 col-xs-12" value="{{$debt->debt_amount}}">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>

              </form>


          </div>
        </div>
      </div>
</div>
@endsection
