@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Update A Reading</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="text-center text-danger">{{$error}}</div>
                @endforeach
            @endif
            <div class="col-sm-12 col-md-12">
                <div class="col-md-8 col-sm-8">
                    <form id="demo-form2"  class="form-horizontal form-label-left"  action="/fuel_pumps_readings/{{$reading->id}}" method="post" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pump_id">Pump <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" id="pump_id" name="pump_id" required>
                                <option value="{{$reading->pump_id}}">{{$reading->pump->pump_name}}</option>
                              </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_id">Product<span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="product_name"  class="form-control col-md-7 col-xs-12" readonly>
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="selling_price_per_litre">Previous Readings<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="number"  class="form-control col-md-7 col-xs-12" id="previous_readings" name="previous_readings" value="{{$reading->previous_readings}}" required>
                            </div>
                          </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="selling_price_per_litre">Selling Price Per Litre<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="number" name="selling_price_per_litre"  id="selling_price_per_litre"  class="form-control col-md-7 col-xs-12" step="0.01" min="0" value="{{$reading->selling_price_per_litre}}">
                            </div>
                          </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="current_readings">Current Readings<span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="current_readings"  id="current_readings"  class="form-control col-md-7 col-xs-12" required step="0.01" min="0" value="{{$reading->current_readings}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>

                      </form>
                </div>
                <div class="col-md-4 col-sm-4">
                    <h4>Pump's Product Image</h4>
                    <img src="" alt="" id="product_image">
                    <h5 class="price" id="in_stock"></h5>
                </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection

@section('custom-scripts')
    {{-- custom jquery --}}
    <script>
 $(document).ready(function(){
    $.get('/api/pump/'+$( "#pump_id").val()+'/product',  // url
                    function (data, textStatus, jqXHR) {
                        if (data.image_url !='') {
                            $("#product_image").attr("src",data.image_url);
                        }else{
                            $("#product_image").attr("src",'/admin/images/no-image.png');
                        }

                        //show amount in stock
                        $('#in_stock').html(data.in_stock+' In Stock');
                        //update the name of the product
                        $('#product_name').val(data.name);
                        $('#selling_price_per_litre').val(data.unit_selling_price);
                });
            //retrive the pump and check if there is an image when making a sale
            $('#pump_id').on('change', function(){
                //check if the id exists
                $.get('/api/pump/'+$( "#pump_id").val()+'/product',  // url
                    function (data, textStatus, jqXHR) {
                        if (data.image_url !='') {
                            $("#product_image").attr("src",data.image_url);
                        }else{
                            $("#product_image").attr("src",'/admin/images/no-image.png');
                        }

                        //show amount in stock
                        $('#in_stock').html(data.in_stock+' In Stock');
                        //update the name of the product
                        $('#product_name').val(data.name);
                        $('#selling_price_per_litre').val(data.unit_selling_price);
                });
                //fetch the current readings
                $.get('/api/fuel_pump/'+$( "#pump_id").val()+'/previous_readings',  // url
                    function (data, textStatus, jqXHR) {
                        let previous_readings;
                        if (data['previos_readings'] == null) {
                            previous_readings = 0;
                        }
                        else{
                            previous_readings = data['previos_readings'].current_readings;
                        }
                        $('#previous_readings').val(previous_readings);
                });
            })
        });
     </script>
@endsection
