@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Stock Details</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="col-md-7 col-sm-7 col-xs-12">
              <div class="product-image">
                  @if ($stock_image != '')
                    <img src="{{$stock_image }}" alt="...">
                  @else
                  <img src="/admin/images/no-image.png" alt="..." width="100" height="400">
                  @endif
              </div>
            </div>

            <div class="col-md-5 col-sm-5 col-xs-12" style="border:0px solid #e5e5e5;">

              <h3 class="prod_title">{{$stock->product->name}}</h3>

              <p> No of items Bought: {{$stock->no_of_items_bought}}</p>
              <h1 class="price"> KES {{$stock->total_amount}}</h1>
              <p> Unit Buying Price: {{$stock->unit_buying_price}}</p>
              <p> Unit selling Price: {{$stock->unit_selling_price}}</p>
              <br>
              <div class="">

                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                          <form action="" method="post" action="{{route('stocks.destroy',$stock->id)}}">
                            @csrf
                            @method('delete')
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Delete the stock of {{$stock->product->name}}?</h4>
                        </div>
                        <div class="modal-body">
                          <p>Are you sure you want to delete  this stock of {{$stock->product->name}}?</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" >Yes Delete</button>
                        </div>
                    </form>
                      </div>
                    </div>
                  </div>
                <button type="button" class="btn btn-warning">Edit</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm">Delete</button>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
