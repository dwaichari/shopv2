@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Fuel Pump Readings List</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of all fuel pump readings
            </p>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Pump Name</th>
                  <th>Product Name</th>
                  <th>Previous Readings</th>
                  <th>Current Readings</th>
                  <th>Litres Sold</th>
                  <th>Buying Price(Lr)</th>
                  <th>Selling Price(Lr)</th>
                  <th>Total Sold</th>
                  <th>Profit</th>
                  <th>Added On</th>
                  <th>Added By</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($readings as $reading)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$reading->pump->pump_name}}</td>
                    <td>{{$reading->product->name}}</td>
                    <td>{{$reading->previous_readings}}</td>
                    <td>{{$reading->current_readings}}</td>
                    <td>{{$reading->litres_sold}}</td>
                    <td>{{$reading->buying_price_per_litre}}</td>
                    <td>{{$reading->selling_price_per_litre}}</td>
                    <td>{{$reading->total_amount}}</td>
                    <td>{{$reading->profit}}</td>
                    <td>{{$reading->created_at}}</td>
                    @if ($reading->user != '')
                    <td>{{$reading->user->name}}</td>
                    @else
                    <td>UNKNOWN</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-danger" href="fuel_pumps_reading/{{$reading->id}}/delete">Delete</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
