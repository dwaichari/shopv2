@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Product Details</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="col-md-7 col-sm-7 col-xs-12">
              <div class="product-image">
                  @if ($product_image != '')
                    <img src="{{$product_image}}" alt="...">
                  @else
                  <img src="/admin/images/no-image.png" alt="..." width="100" height="400">
                  @endif
              </div>
            </div>

            <div class="col-md-5 col-sm-5 col-xs-12" style="border:0px solid #e5e5e5;">

              <h3 class="prod_title">{{$product->name}}</h3>

              <p>{{$product->description}}</p>
              <br>
              <h2> No of Items In Stock: {{$product->in_stock}}</h2>
              <h2> Product Code: {{$product->id}}</h2>
              <h2 class="price"> Unit selling Price: <span class="price"> KES {{$product->unit_selling_price}}</span></h2>
              <div class="">

                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                          <form action="" method="post" action="{{route('products.destroy',$product->id)}}">
                            @csrf
                            @method('delete')
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Delete {{$product->name}}</h4>
                        </div>
                        <div class="modal-body">
                          <p>Are you sure you want to delete {{$product->name}}?</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" >Yes Delete</button>
                        </div>
                    </form>
                      </div>
                    </div>
                  </div>
                <a type="button" class="btn btn-warning" href="/products/{{$product->id}}/edit">Edit</a>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm">Delete</button>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
