@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <div class="row">
              <form action="/products_performance" method="get">
              <div class="col-md-6">
                <h4>Products Performace List : <small>{{$dateRangeString}}</small></h4>
              </div>
              <div class="col-md-4">
                <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                  <span id="dateRange">{{$dateStringText}}</span> <b class="caret"></b>
                  <input type="hidden" id="dateRangeText" name="dateRangeText">
                  </div>
              </div>
              <div class="col-md-2">
                <button type="submit" class="btn btn-success btn-sm">Filter</button>
              </div>
            </form>
            </div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of products and their performances
            </p>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Stock Value</th>
                  <th>Total Sales</th>
                  <th>Total Profits</th>
                  <th>Description</th>
                  <th>Unit Buying Price</th>
                  <th>Unit Selling Price</th>
                  <th>Items In Stock</th>
                  <th>Added On</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($products as $product)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$product->id}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->stock_value}}</td>
                    <td>{{$product->total_sales}}</td>
                    <td>{{$product->total_profit}}</td>
                    <td>{{$product->description}}</td>
                    <td>{{$product->unit_buying_price}}</td>
                    <td>{{$product->unit_selling_price}}</td>
                    <td>{{$product->in_stock}}</td>
                    <td>{{$product->created_at}}</td>
                    <td>
                        <a class="btn btn-sm btn-info" href="products/{{$product->id}}">Manage</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
@section('custom-scripts')
<script>
$(document).ready(function(){

    $("#dateRange").on('DOMSubtreeModified',function(){
        $('#dateRangeText').val($("#dateRange").text());
    });
});
</script>
@endsection
