@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Edit Product</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="text-center text-danger">{{$error}}</div>
                @endforeach
            @endif
            <form id="demo-form2"  class="form-horizontal form-label-left"  action="/products/{{$product->id}}" method="post" enctype="multipart/form-data" autocomplete="off">
                @csrf
                @method('PUT')
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name">Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="name"  id="product_name" required="required" class="form-control col-md-7 col-xs-12" value="{{$product->name}}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit_selling_price">Unit Selling Price <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="number" name="unit_selling_price"  id="unit_selling_price" required="required" class="form-control col-md-7 col-xs-12" value="{{$product->unit_selling_price}}" required step="0.01">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="in_stock">Items In Stock <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="number" name="in_stock"  id="in_stock" required="required" class="form-control col-md-7 col-xs-12" value="{{$product->in_stock}}" required step="0.01">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="description" name="description"  class="form-control" >{{$product->description}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Picture</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" name="picture">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>

              </form>


          </div>
        </div>
      </div>
</div>
@endsection
