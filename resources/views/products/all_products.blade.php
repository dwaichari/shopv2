@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Products List</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of products available in the database
            </p>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Unit Buying Price</th>
                  <th>Unit Selling Price</th>
                  <th>Items In Stock</th>
                  <th>Added On</th>
                  <th>Added By</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($products as $product)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$product->id}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->description}}</td>
                    <td>{{$product->unit_buying_price}}</td>
                    <td>{{$product->unit_selling_price}}</td>
                    <td>{{$product->in_stock}}</td>
                    <td>{{$product->created_at}}</td>
                    @if ($product->user != '')
                    <td>{{$product->user->name}}</td>
                    @else
                    <td>UNKNOWN</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-info" href="products/{{$product->id}}">Manage</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
