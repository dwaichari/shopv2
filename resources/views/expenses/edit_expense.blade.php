@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Edit Expense</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="text-center text-danger">{{$error}}</div>
                @endforeach
            @endif
            <form id="demo-form2"  class="form-horizontal form-label-left"  action="/expenses/{{$expense->id}}" method="post" enctype="multipart/form-data" autocomplete="off">
                @csrf
                @method('PUT')
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Expense Type <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control" id="type" name="type" required>
                        <option value="">Choose expense</option>
                        <option value="Business" {{$expense->type =='Business'? 'selected':''}}>Business</option>
                        <option value="Home"{{$expense->type =='Home'? 'selected':''}}>Home</option>
                        <option value="General"{{$expense->type =='General'? 'selected':''}}>General</option>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="description" name="description"  class="form-control" >{{$expense->description}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Amount <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="amount"  id="amount" required="required" class="form-control col-md-7 col-xs-12"  step="0.01" value="{{$expense->amount}}">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>

              </form>


          </div>
        </div>
      </div>
</div>
@endsection
