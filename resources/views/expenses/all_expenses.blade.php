@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <div class="row">
              <div class="col-md-6">
                <h2>Expenses List | Total Amount: {{$expenses->sum('amount')}} | Timeline: {{$timeline}}</h2>
                <h2>Grand Business Profit: {{$profit}} | Net Profit(After expenses): {{$net_profit}}</h2>
              </div>
              <div class="col-md-5">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                  <span id="date_span"></span> <b class="caret"></b>
                  </div>
              </div>
              <div class="col-md-1">
                <form action="#" method="GET">
                  <input type="text"  name="date_range" id="date_range" hidden>
                  <button type="submit" class="btn btn-primary btn-sm">Filter</button>
                </form>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              List of Expenses available in the database
            </p>

            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" height="100%">
              <thead>
                <tr>
                  <th class="text-center">S/N</th>
                  <th>Type</th>
                  <th>Description</th>
                  <th>Amount</th>
                  <th>Added On</th>
                  <th>Added By</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($expenses as $expense)
                  <tr>
                    <td class="text-center">{{$loop->index + 1}}</td>
                    <td>{{$expense->type}}</td>
                    <td>{{$expense->description}}</td>
                    <td>{{$expense->amount}}</td>
                    <td>{{$expense->created_at}}</td>
                    @if ($expense->user != '')
                    <td>{{$expense->user->name}}</td>
                    @else
                    <td>UNKNOWN</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-info" href="expenses/{{$expense->id}}">Manage</a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>


          </div>
        </div>
      </div>
</div>
@endsection
@section('custom-scripts')
    {{-- custom jquery --}}
    <script>
$(document).ready(function(){
  // Get the text of the span and set it as the value of the input field
  var dateSpan = $('#date_span');
  var dateInput = $('#date_range');
  dateInput.val(dateSpan.text());

  // Listen for changes to the span and update the input accordingly
  dateSpan.on('DOMSubtreeModified', function() {
    dateInput.val(dateSpan.text());
  });
});
</script>
@endsection