require('./bootstrap');

window.Vue = require('vue').default;
import 'vue-search-select/dist/VueSearchSelect.css';

Vue.component('sale-component', require('./components/SaleComponent.vue').default);

const app = new Vue({
    el: '#main'
});
