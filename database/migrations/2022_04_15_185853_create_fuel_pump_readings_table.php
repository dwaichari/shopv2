<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_pump_readings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('pump_id')->unsigned();
            $table->bigInteger('previous_readings')->default(0);
            $table->bigInteger('current_readings')->default(0);
            $table->double('litres_sold')->default(0);
            $table->double('buying_price_per_litre')->default(0);
            $table->double('selling_price_per_litre')->default(0);
            $table->double('total_amount')->default(0);
            $table->double('profit')->default(0);
            $table->foreign('pump_id')->references('id')->on('fuel_pumps')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_pump_readings');
    }
};
