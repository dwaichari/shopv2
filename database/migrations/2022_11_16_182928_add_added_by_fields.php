<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //create an array of strings
        $tablesTofill = ['products','stocks','sales','fuel_pumps','fuel_pump_readings'];

        foreach ($tablesTofill as $entry) {
            Schema::table($entry, function (Blueprint $table) {
                $table->unsignedBigInteger('added_by')->nullable()->after('updated_at');
                $table->foreign('added_by')->references('id')->on('users')->onDelete('SET NULL');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //create an array of strings
        $tablesToDrop = ['products','stocks','sales','fuel_pumps','fuel_pump_readings'];
        foreach ($tablesToDrop as $entry) {
            Schema::table($entry, function (Blueprint $table) {
                // 1. Drop foreign key constraints
                $table->dropForeign(['added_by']);
   
                // 2. Drop the column
                $table->dropColumn('added_by');
           });
        }
    }
};
