<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
  
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           'can-view-roles',
           'can-create-roles',
           'can-edit-role',
           'can-delete-role',
           'can-view-products',
           'can-create-products',
           'can-edit-product',
           'can-delete-product',
           'can-view-stocks',
           'can-create-stock',
           'can-edit-stock',
           'can-delete-stock',
           'can-view-sales',
           'can-create-sale',
           'can-edit-sale',
           'can-delete-sale',
           'can-view-users',
           'can-create-user',
           'can-edit-users',
           'can-delete-users',
           'can-view-fuel-pumps',
           'can-create-fuel-pumps',
           'can-edit-fuel-pump',
           'can-delete-fuel-pumps',
           'can-view-fuel-pumps-readings',
           'can-create-fuel-pump-reading',
           'can-edit-fuel-pump-reading',
           'can-delete-fuel-pump-reading',
           'can-view-business-report',
           'can-view-dashboard',
           'can-see-admin-section',
        ];
        
        $allpermissions =Permission::all();
        foreach ($allpermissions as $perm) {
            $perm->delete();
        }
        foreach ($permissions as $permission) {
            //check if the permission exists otherwise create the permission
            $existing_permission = Permission::where('name', $permission)->first();

            if ($existing_permission == ''){
                Permission::create(['name' => $permission]);
            }
        }
    }
}