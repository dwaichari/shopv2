<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
  
class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //if we dont have a user create a new user otherwise use the first user as the admin
        //check if there is a user in the database
        $user = User::where('email','admin@website.com')->first(); 

        $roles = Role::all(); 
        $role_found = false;
        $role = null;
        if ($roles->count() >=1) {
            foreach ($roles as $role) {
                if ($role_found == false) {
                    //we update we have found a role
                    $role_found == true;
                    $role = $role;
                }
            }
        }
        else{
            $role = Role::create(['name' => 'Admin']);
        }
    
        $permissions = Permission::pluck('id','id')->all();
   
        $role->syncPermissions($permissions);
     
        $user->assignRole([$role->id]);
    }
}