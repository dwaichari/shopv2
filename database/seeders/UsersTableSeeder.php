<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;



class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //check if david exists
        $user = User::where('email', 'admin@website.com')->first();

        if ($user == '') {
            DB::table('users')->insert([
                'name'=>'David Waichari',
                'email'=>'admin@website.com',
                'password' => Hash::make('password'),
            ]);
        }
    }
}
