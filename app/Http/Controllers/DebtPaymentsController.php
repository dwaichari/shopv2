<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DebtPayment;
use App\Models\Customer;
use Auth;

class DebtPaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //check if there is a customer_id on the request
        if ($request->customer_id) {
            //get the debt_payments only for this customer
            $debt_payment_title = Customer::find($request->customer_id)->name;

            $debt_payments = DebtPayment::where('customer_id', $request->customer_id)->get()->sortByDesc("created_at");
            
        }
        else{
            //return all debt_payments
            $debt_payment_title = "All Customers";
            $debt_payments = DebtPayment::all()->sortByDesc("created_at");
        }
        $debt_payment_amount = $debt_payments->sum('debt_payment_amount');

        return view('debt_payments/all_debt_payments',compact('debt_payments', 'debt_payment_title', 'debt_payment_amount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        return view('debt_payments/create_debt_payment', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valiator = $request->validate([
            'amount_paid' => 'required',
            'customer_id' => 'required'
        ]);

        $request['added_by'] = Auth::user()->id;
        $debt_payment = DebtPayment::create($request->all());

        //update the customer
        $customer = Customer::find($request->customer_id);
        $customer->debt_amount -= $request->amount_paid;
        $customer->save();
        return redirect('/debt_payments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $debt_payment = DebtPayment::findOrFail($id);

        return view('debt_payments.debt_payment_details', compact('debt_payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $debt_payment = DebtPayment::findOrFail($id);
        $customers = Customer::all();
        return view('debt_payments.edit_debt_payment',compact('debt_payment', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $valiator = $request->validate([
        //     'debt_payment_amount' => 'required',
        // ]);
        // $debt_payment = DebtPayment::findOrFail($id);
        // $request['customer_id'] = $debt_payment->customer_id;
        // $debt_payment ->update($request->all());

        // return redirect('/debt_payments/'.$debt_payment->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $debt_payment = DebtPayment::findOrFail($id);
        
        $debt_payment->delete();
         //update the customer
         $customer = Customer::find($debt_payment->customer_id);
         $customer->debt_amount += $debt_payment->amount_paid;
         $customer->save();
         
        return redirect('/debt_payments');
    }

    public function debt_paymentsByCustomers()
    {
       $customers = Customer::all();

       return view('debt_payments.customers',compact('customers'));
    }

}
