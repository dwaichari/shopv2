<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FuelPumpReading;
use App\Models\FuelPump;
use App\Models\Product;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SalesController;
use DB;

class FuelPumpReadingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:can-view-fuel-pumps-readings|can-create-fuel-pump-reading|can-edit-fuel-pump-reading|can-delete-fuel-pump-reading', ['only' => ['index','store']]);
         $this->middleware('permission:can-create-fuel-pump-reading', ['only' => ['create','store']]);
         $this->middleware('permission:can-edit-fuel-pump-reading', ['only' => ['edit','update']]);
         $this->middleware('permission:can-delete-fuel-pump-reading', ['only' => ['destroy']]);
    }



    public function index()
    {
        $readings = FuelPumpReading::all()->sortByDesc("created_at");

        return view('fuel_pumps_readings.all_pumps_readings', compact('readings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pumps = FuelPump::all();

        return view('fuel_pumps_readings.add_fuel_pump_readings',compact('pumps'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pump = FuelPump::findOrFail($request->pump_id);
        $product = $pump->product;
        $previos_readings = 0;

        //fetch the previous readings
         $last_reading = DB::table('fuel_pump_readings')->where('pump_id', $pump->id)->latest()->first();

         if ($last_reading != '') {
            $previos_readings = $last_reading->current_readings;
         }
         //amount sold
         $amount_sold = $request->current_readings - $previos_readings;
         //calculate the litres sold
        $litres_sold = $amount_sold / $request->selling_price_per_litre;
        //check if the litres are more than the available items
        if ($litres_sold > $product->in_stock) {
            return Redirect::back()->withErrors(['', 'You cannot sell more than you have. please check the readings again']);
        }
        //check if the current readings are less than the previous readings
        if ($request->current_readings <= $previos_readings) {
            return Redirect::back()->withErrors(['', 'The current readings should be greator than the previous readings. confirm']);
        }
        //profit
        $profit = ( $litres_sold * $request->selling_price_per_litre) - ( $litres_sold * $product->unit_buying_price);
        //save the reading
        $reading = new FuelPumpReading();
        $reading->product_id= $product->id;
        $reading->pump_id = $pump->id;
        $reading->previous_readings = $previos_readings;
        $reading->current_readings = $request->current_readings;
        $reading->litres_sold =  $litres_sold;
        $reading->buying_price_per_litre = $product->unit_buying_price;
        $reading->selling_price_per_litre = $request->selling_price_per_litre;
        $reading->total_amount = $litres_sold * $request->selling_price_per_litre;
        $reading->profit = $profit;

        $reading->save();

        //call the checkout method on sales controller
        $sale_request = new Request();
        $sale_request['product_id'] = $product->id;
        $sale_request['no_of_items'] = $litres_sold;
        $sale_request['unit_buying_price'] = $reading->buying_price_per_litre;
        $sale_request['unit_selling_price'] = $reading->selling_price_per_litre;

        //make a sale
        return (new SalesController)->checkout($sale_request);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->back();
        //edit readings
        $reading = FuelPumpReading::findOrFail($id);
        $pumps = FuelPump::all();
        return view('fuel_pumps_readings.edit_fuel_pump_readings', compact('reading','pumps'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $pump = FuelPump::findOrFail($request->pump_id);
        // $product = $pump->product;
        // $previos_readings = 0;

        // // //fetch the previous readings
        // //  $last_reading = $pump->fuelpumpsreadings->last();

        // //  if ($last_reading != '') {
        // //     $previos_readings = $last_reading->current_readings;
        // //  }

        // //  //calculate the litres sold
        // $litres_sold = $request->current_readings - $request->previous_readings;
        // // //check if the litres are more than the available items
        // // if ($litres_sold > $product->in_stock) {
        // //     return Redirect::back()->withErrors(['', 'You cannot sell more than you have. please check the readings again']);
        // // }
        // // //check if the current readings are less than the previous readings
        // // if ($request->current_readings <= $previos_readings) {
        // //     return Redirect::back()->withErrors(['', 'The current readings should be greator than the previous readings. confirm']);
        // // }
        // //profit
        // $profit = ( $litres_sold * $request->selling_price_per_litre) - ( $litres_sold * $product->unit_buying_price);
        // //save the reading
        // $reading = FuelPumpReading::findOrFail($id);
        // //store the initial number of litres
        // $initial_litres = $reading->litres_sold;

        // $reading->product_id= $product->id;
        // $reading->pump_id = $pump->id;
        // $reading->previous_readings = $request->previous_readings;
        // $reading->current_readings = $request->current_readings;
        // $reading->litres_sold =  $litres_sold;
        // $reading->buying_price_per_litre = $product->unit_buying_price;
        // $reading->selling_price_per_litre = $request->selling_price_per_litre;
        // $reading->total_amount = $litres_sold * $request->selling_price_per_litre;
        // $reading->profit = $profit;

        // $reading->save();
        // // //reduce update stock
        // $stock_before_sale = $initial_litres + $product->in_stock;
        // $product->in_stock -= $stock_before_sale - $litres_sold;
        // $product->save();

        // //call the checkout method on sales controller
        // $sale_request = new Request();
        // $sale_request['product_id'] = $product->id;
        // $sale_request['no_of_items'] = $litres_sold;
        // $sale_request['unit_buying_price'] = $reading->buying_price_per_litre;
        // $sale_request['unit_selling_price'] = $reading->selling_price_per_litre;

        // //make a sale
        // return (new SalesController)->checkout($sale_request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FuelPumpReading::findOrFail($id)->delete();
        return redirect('/fuel_pumps_readings');
    }

     //fetch previous readings for a pump
     public function ajaxFetchPumpPreviousReadings($pump_id)
     {

        $last_reading = DB::table('fuel_pump_readings')->where('pump_id', $pump_id)->latest()->first();
         return response()->json($last_reading, 200, []);
     }
}
