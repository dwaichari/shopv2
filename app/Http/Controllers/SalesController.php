<?php

namespace App\Http\Controllers;
use App\Models\Sale;
use App\Models\ItemSale;
use App\Models\Product;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Auth;

use Illuminate\Http\Request;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:can-view-sales|can-create-sale|can-edit-sale|can-delete-sale', ['only' => ['index','store']]);
         $this->middleware('permission:can-create-sale', ['only' => ['create','store','checkOutForm','checkout','confirmSale','removeSalesItem']]);
         $this->middleware('permission:can-edit-sale', ['only' => ['edit','update']]);
         $this->middleware('permission:can-delete-sale', ['only' => ['destroy']]);
    }


    public function index()
    {

        $sales = Sale::all()->sortByDesc("created_at");

        return view('sales.all_sales',compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_products = Product::all();
        $products = [];

        foreach ($all_products as $product) {
            if ($product->fuelpumps->count() == 0) {
                array_push($products, $product);
            }
        }

        return view('sales.add_sale',compact('products'));
    }

    public function checkOutForm($sale_id)
    {
        //fetch sale
        $sale = Sale::findOrFail($sale_id);
        //fetch the sale items
        $sale_items = $sale->itemsales;
        $all_products = Product::all();
        $products = [];

        foreach ($all_products as $product) {
            if ($product->fuelpumps->count() == 0) {
                array_push($products, $product);
            }
        }
       //return checkout view
       return view('sales.add_sale_checkout',compact('sale','sale_items','products'));
    }
    public function checkout(Request $request)
    {
        //check the number of items available in stock
        if ($request->product_id !='') {
            $product = Product::findOrFail($request->product_id);
            if ($request->no_of_items > $product->in_stock ) {
                return Redirect::back()->withErrors(['', 'Sale Rejected, check the items in stock']);
            }
            if ($request->no_of_items ==0 ) {
                return Redirect::back()->withErrors(['', 'You cannot sell 0 items']);
            }
        }
       //create a sale if sale does not exist
       if ($request->sale_id != '') {
           $sale = Sale::findOrFail($request->sale_id);
       }else{
           $sale = Sale::create();
           //update the added by field
           $sale->added_by = Auth::user()->id;
       }

       //save the current item if the product is available
       if ($request->product_id !='') {
           //validate record
           $valiator = $request->validate([
            'product_id' => 'required|integer',
            'no_of_items' => 'required',
             ]);

           $item_sale = new ItemSale();
           $item_sale->sale_id= $sale->id;
            $item_sale->product_id = $request->product_id;
            $item_sale->unit_buying_price = $request->unit_buying_price;
            $item_sale->unit_selling_price = $request->unit_selling_price;
            $item_sale->profit = ($item_sale->unit_selling_price *$request->no_of_items) - ($item_sale->unit_buying_price *$request->no_of_items);
            $item_sale->no_of_items = $request->no_of_items;
            $item_sale->total_buying_price = $request->no_of_items * $request->unit_buying_price;
            $item_sale->total_selling_price = $request->no_of_items * $request->unit_selling_price;
            $item_sale->save();

            //reduce the number of items in the product
            $product = $item_sale->product;
            $product->in_stock -= $item_sale->no_of_items;
            $product->save();
       }
        //update sale
        $sale ->customer_name = $request->customer_name;
        $sale->total_buying_price = $sale->itemsales->sum('total_buying_price');
        $sale->total_selling_price = $sale->itemsales->sum('total_selling_price');
        $sale->balance = $sale->cash_received - $sale->total_selling_price;
        $sale->total_profit = $sale->itemsales->sum('profit');

        $sale->save();
        return redirect('sales_checkout/'.$sale->id);
    }
    public function confirmSale(Request $request)
    {
        $sale = Sale::findOrFail($request->sale_id);
        $sale->cash_received = $request->cash_received;
        $sale->balance = $request->balance;

        $sale->status = 'confimed';
        $sale->save();

        return redirect('/sales');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function removeSalesItem($item_sale_id)
     {
         //remove item
         $sale_item = ItemSale::findOrFail($item_sale_id);
         $sale = $sale_item->sale;
         $product = Product::findOrFail($sale_item->product_id);

         //update items in stock for this product
         $product->in_stock += $sale_item->no_of_items;
         $product->save();

         $sale_item->delete();

        //redirect to checkout with no product to update the sale
        $request = new Request();
        $request['product_id'] = '';
        $request['sale_id'] = $sale->id;
        //update the balance

        //call the checkout method
        return $this->checkout($request);
     }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $sale = Sale::findOrFail($id);
        //add stock to the product for this sale
        $sale_items = $sale->itemsales;
        if ($sale_items->count() >0) {
            foreach ($sale_items as $sale_item) {
               $product = Product::findOrFail($sale_item->product_id);
               $product->in_stock +=$sale_item->no_of_items;
               $product->save();
            }
        }
        $sale->delete();
        return redirect('/sales');
    }

    public function ajaxGetChartData(Request $request)
    {
        $months = [];
        $sales = [];
        $items_sold = [];
        $profits = [];
         //check if there is a product in the request body
        if ($request->product_id != '') {
            $product = Product::findOrFail($request->product_id);
        }else{
            $product = 'noProduct';
        }
       
        for ($i=12; $i >= 0; $i--) {
            $month = Carbon::now()->subMonth($i);
            //check if there is a product already
            if ($product !='noProduct') {
                //use itemSale instead
                $all_sales = ItemSale::whereYear('created_at', $month->year)->whereMonth('created_at', $month->month)->where('product_id',$product->id)->get();
                array_push($sales, $all_sales->sum('total_selling_price'));
                array_push($items_sold, $all_sales->sum('no_of_items'));
                array_push($profits,$all_sales->sum('profit'));
                array_push($months, $month->format('M Y'));
            } else {
                $all_sales = Sale::whereYear('created_at', $month->year)->whereMonth('created_at', $month->month)->get();
                //compute the number of items
                $no_of_items =0;
                foreach ($all_sales as $sale) {
                    $no_of_items+= $sale->itemsales->sum('no_of_items');
                }
                array_push($sales, $all_sales->sum('total_selling_price'));
                array_push($items_sold, $no_of_items);
                array_push($profits,$all_sales->sum('total_profit'));
                array_push($months, $month->format('M Y'));
            }
        }
        
        $response = [
            'product'=>$product,
            'months' => $months,
            'sales' =>$sales,
            'profits' => $profits,
            'items_sold' => $items_sold
        ];
        return response()->json($response, 200, []);
    }
}
