<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Models\Sale;
use App\Models\FuelPump;
use App\Models\ItemSale;
use App\Models\Stock;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:can-view-dashboard', ['only' => ['index']]);
        $this->middleware('permission:can-view-business-report', ['only' => ['businessPerfomance','productsPerfomance']]);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Product::all();
        $users = User::all();
        $sales_today = Sale::whereDate('created_at', Carbon::today())->get();
        $fuel_pumps = FuelPump::all();
        $sales = Sale::all()->sortByDesc("created_at");
        return view('home',compact('products','users','sales_today','fuel_pumps','sales'));
    }

    public function businessPerfomance(Request $request)
    {

        $productStringText = 'All Products';
        $stock_value = 0;
        $projected_sales = 0;
        $projected_profit = 0;
        $products = Product::all();
        $sales = [];
        $dateRangeString = '';
        $dateStringText =$request->dateRangeText;
        //check if there is a filter date
        if ($dateStringText != '') {
            $all_sales = Sale::all();

            $startDate  =explode('-',$dateStringText)[0];
            $endDate =explode('-',$dateStringText)[1];
            $startDateFormated = Carbon::create($startDate);
            $endDateFormated = Carbon::create($endDate); //April 30, 2022
            if ($all_sales->count() > 0) {
                $sales = Sale::whereDate('created_at', '>=',$startDateFormated)
                ->whereDate('created_at', '<=', $endDateFormated)->get();
            }
            $dateRangeString = 'Sales from '.$startDate.' to '.$endDate;
        }else{
            $sales = Sale::all()->sortByDesc("created_at");
            $dateRangeString = 'All Time Sales List';
        }
        //check if there is a product filter
        if ($request->product_id != '') {
            
            $product = Product::findOrFail($request->product_id);
            //change the stock value only for this product
            $stock_value = $product->in_stock * $product->unit_buying_price;
            $projected_sales += $product->in_stock * $product->unit_selling_price;
            $filteredSales = [];
            //overwrite products string text
            $productStringText  = $product->name;
            //overwrite sales
            
            foreach ($sales as $sale) {
                $itemSale =ItemSale::where([
                    'product_id' => $product->id,
                    'sale_id' => $sale->id
             ])->first();
             //check if the sale for this product exists
             if ($itemSale != null) {
                $sale->total_buying_price = $itemSale ->total_buying_price;
                $sale->total_selling_price = $itemSale ->total_selling_price;
                $sale->total_profit = $itemSale ->profit;
                $sale->balance = 0;
                $sale->cash_received = $itemSale ->total_selling_price;
 
                array_push($filteredSales, $sale);
             }
            }

            $sales = collect($filteredSales);

        }
        else{
            //calculate stock value and projected sales of all products
            foreach ($products as $product) {
                $stock_value += $product->in_stock * $product->unit_buying_price;
                $projected_sales += $product->in_stock * $product->unit_selling_price;
            }
        }
        //calculate the projected profit
        $projected_profit = $projected_sales - $stock_value;

        return view('sales.business_performace',compact('sales','stock_value','projected_sales','products','productStringText', 'dateRangeString','dateStringText', 'projected_profit'));
    }

    public function productsPerfomance(Request $request)
    {
         //return all products
         $all_products = Product::all()->sortByDesc("created_at");
         $products = [];
         $dateRangeString = '';
        foreach ($all_products as $product) {
            
            $dateRangeString = '';
            $dateStringText =$request->dateRangeText;
            $product_stocks = $product->stocks;
            $product_sales = $product->itemsales;
            $dateRangeString = 'All Time product performance';
            //check if there is a filter date
            if ($dateStringText != '') {
                $startDate  =explode('-',$dateStringText)[0];
                $endDate =explode('-',$dateStringText)[1];
                $startDateFormated = Carbon::create($startDate);
                $endDateFormated = Carbon::create($endDate); //April 30, 2022
                if ($product_sales->count() > 0) {
                    $product_sales = ItemSale::whereDate('created_at', '>=',$startDateFormated)
                    ->whereDate('created_at', '<=', $endDateFormated)->where('product_id',$product->id)->get();
                }
                $dateRangeString = 'performance from '.$startDate.' to '.$endDate;
            }
            $product['stock_value'] = $product->in_stock * $product->unit_buying_price;
            $product['total_sales'] = $product_sales->sum('total_selling_price');
            $product['total_profit'] = $product_sales->sum('profit');

            array_push($products, $product);
        }

        //last six months
        
         return view('products/products_performance',compact('products','dateRangeString','dateStringText'));
    }

    public function showNoPermissionPage()
    {
        return view('unauthorized');
    }
}
