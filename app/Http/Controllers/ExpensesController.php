<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Expense;
use Auth;
use Carbon\Carbon;
use App\Models\Sale;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // function __construct()
    // {
    //      $this->middleware('permission:can-view-expenses|can-create-expenses|can-edit-expense|can-delete-expense', ['only' => ['index','store']]);
    //      $this->middleware('permission:can-create-expenses', ['only' => ['create','store']]);
    //      $this->middleware('permission:can-edit-expense', ['only' => ['edit','update']]);
    //      $this->middleware('permission:can-delete-expense', ['only' => ['destroy']]);
    // }

    public function index(Request $request)
    {
        //check if there is a date filter
        if (request()->input('date_range')) {
            # code...
            $dateRange = request()->input('date_range');
            $dates = explode(' - ', $dateRange);
    
            $startDate = Carbon::parse($dates[0])->startOfDay();
            $endDate = Carbon::parse($dates[1])->endOfDay();
    
            $expenses = Expense::whereBetween('created_at', [$startDate, $endDate])->get();
            $timeline =$startDate->format('M j, Y').' to '.$endDate->format('M j, Y');
            $profit = Sale::whereBetween('created_at', [$startDate, $endDate])->get()->sum('total_profit');
            
        }else{
            $expenses = Expense::all()->sortByDesc("created_at");
            $profit = Sale::all()->sum('total_profit');
            $timeline = 'All Time';
        }
       
        $net_profit = $profit - $expenses->sum('amount');
        //return all expenses
        
        return view('expenses/all_expenses',compact('expenses','timeline','profit','net_profit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('expenses/create_expense');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valiator = $request->validate([
            'type' => 'required',
            'description' => 'required',
        ]);

        $request['added_by'] = Auth::user()->id;
        $expense = Expense::create($request->all());

        return redirect('/expenses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expense = Expense::findOrFail($id);

        return view('expenses.expense_details', compact('expense'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = Expense::findOrFail($id);
        return view('expenses.edit_expense',compact('expense'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valiator = $request->validate([
            'type' => 'required',
            'description' => 'required',
        ]);
        $expense = Expense::findOrFail($id);
        $expense ->update($request->all());
        return redirect('/expenses/'.$expense->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = Expense::findOrFail($id);
        $expense->delete();
        return redirect('/expenses');
    }
}
