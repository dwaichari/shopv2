<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stock;
use App\Models\Product;
use App\Models\ItemSale;
use Auth;

class StocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
         $this->middleware('permission:can-view-stocks|can-create-stock|can-edit-stock|can-delete-stock', ['only' => ['index','store']]);
         $this->middleware('permission:can-create-stock', ['only' => ['create','store']]);
         $this->middleware('permission:can-edit-stock', ['only' => ['edit','update']]);
         $this->middleware('permission:can-delete-stock', ['only' => ['destroy']]);
    }


    public function index()
    {
        $stocks = Stock::all()->sortByDesc("created_at");

        return view('stocks.all_stocks', compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();

        return view('stocks.add_stock', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valiator = $request->validate([
            'product_id' => 'required|integer',
            'no_of_items_bought' => 'required',
            'total_amount' => 'required',
            'unit_buying_price' => 'required',
            'unit_selling_price' => 'required',
        ]);
        //save the stocks in stocks
        $request['added_by'] = Auth::user()->id;
        $stock = Stock::create($request->all());
        //save the receipt
        if($request->hasFile('picture') && $request->file('picture')->isValid()){
            $stock->addMediaFromRequest('picture')->toMediaCollection('stock_receipts');
        }
        //update product
        $product = $stock->product;
        $product->unit_buying_price = $request->unit_buying_price;
        $product->unit_selling_price = $request->unit_selling_price;
        $product->in_stock +=$request->no_of_items_bought;
        $product->save();

        return redirect('/stocks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stock = Stock::findOrFail($id);

        $stock_image =$stock->getFirstMediaURL('stock_receipts');
        return view('stocks.stock_details',compact('stock','stock_image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stock = Stock::findOrFail($id);
        return view('stocks.edit_stock', compact('stock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stock = Stock::findOrFail($id);
        //update stock
        $valiator = $request->validate([
            'product_id' => 'required|integer',
            'no_of_items_bought' => 'required',
            'total_amount' => 'required',
            'unit_buying_price' => 'required',
            'unit_selling_price' => 'required',
        ]);
        //save the stocks in stocks
        $stock->update($request->all());
        //save the receipt
        if($request->hasFile('picture') && $request->file('picture')->isValid()){
            $stock->clearMediaCollection('stock_receipts');
            $stock->addMediaFromRequest('picture')->toMediaCollection('stock_receipts');
        }

        //fetch sales for the product
        $sold_items = 0;
        $sales = ItemSale::where('product_id', $request->product_id)->get();
        //check if there are sales
        if ($sales->count() > 0) {
            $sold_items = $sales->sum('no_of_items');
        }

        //calculate all stock items
        $stock_count = 0;
        $stocks_available = Stock::where('product_id',$request->product_id)->get();
        if ($stocks_available->count() >0) {
            $stock_count = $stocks_available->sum('no_of_items_bought');
        }
        //update product
        $product = $stock->product;
        $product->unit_buying_price = $request->unit_buying_price;
        $product->unit_selling_price = $request->unit_selling_price;
        $product->in_stock = $stock_count - $sold_items;
        $product->save();

        return redirect('/stocks/'.$stock->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = Stock::findOrFail($id);
        $stock->delete();
        //check if there is a receipt
        $stock_image = $stock->getFirstMediaURL('stock_receipts');
        if ($stock_image !='') {
            $stock->clearMediaCollection('stock_receipts');
        }

        //fetch sales for the product
        $sold_items = 0;
        $sales = ItemSale::where('product_id', $stock->product_id)->get();
        //check if there are sales
        if ($sales->count() > 0) {
            $sold_items = $sales->sum('no_of_items');
        }

        //calculate all stock items
        $stock_count = 0;
        $stocks_available = Stock::where('product_id',$stock->product_id)->get();
        if ($stocks_available->count() >0) {
            $stock_count = $stocks_available->sum('no_of_items_bought');
        }
        //update product
        $product = $stock->product;
        $product->in_stock = $stock_count - $sold_items;
        $product->save();



        return redirect('/stocks');
    }
}
