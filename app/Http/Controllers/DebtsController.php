<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Debt;
use App\Models\Customer;
use Auth;

class DebtsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    public function index(Request $request)
    {
        //check if there is a customer_id on the request
        if ($request->customer_id) {
            //get the debts only for this customer
            $debt_title = Customer::find($request->customer_id)->name;

            $debts = Debt::where('customer_id', $request->customer_id)->get()->sortByDesc("created_at");
            
        }
        else{
            //return all debts
            $debt_title = "All Customers";
            $debts = Debt::all()->sortByDesc("created_at");
        }
        $debt_amount = $debts->sum('debt_amount');

        return view('debts/all_debts',compact('debts', 'debt_title', 'debt_amount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        return view('debts/create_debt', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valiator = $request->validate([
            'debt_amount' => 'required',
            'customer_id' => 'required'
        ]);

        $request['added_by'] = Auth::user()->id;
        $debt = Debt::create($request->all());

        //update the customer
        $customer = Customer::find($request->customer_id);
        $customer->debt_amount += $request->debt_amount;
        $customer->save();
        return redirect('/debts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $debt = Debt::findOrFail($id);

        return view('debts.debt_details', compact('debt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $debt = Debt::findOrFail($id);
        $customers = Customer::all();
        return view('debts.edit_debt',compact('debt', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $valiator = $request->validate([
        //     'debt_amount' => 'required',
        // ]);
        // $debt = Debt::findOrFail($id);
        // $request['customer_id'] = $debt->customer_id;
        // $debt ->update($request->all());

        // return redirect('/debts/'.$debt->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $debt = Debt::findOrFail($id);
        
        $debt->delete();
         //update the customer
         $customer = Customer::find($debt->customer_id);
         $customer->debt_amount -= $debt->debt_amount;
         $customer->save();
         
        return redirect('/debts');
    }

    public function debtsByCustomers()
    {
       $customers = Customer::where('debt_amount', '>', '0')->get()->sortByDesc("debt_amount");

       return view('debts.customers',compact('customers'));
    }

}
