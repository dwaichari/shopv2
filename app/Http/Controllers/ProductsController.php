<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Auth;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:can-view-products|can-create-products|can-edit-product|can-delete-product', ['only' => ['index','store']]);
         $this->middleware('permission:can-create-products', ['only' => ['create','store']]);
         $this->middleware('permission:can-edit-product', ['only' => ['edit','update']]);
         $this->middleware('permission:can-delete-product', ['only' => ['destroy']]);
    }

    public function index()
    {
        //return all products
        $products = Product::all()->sortByDesc("created_at");

        return view('products/all_products',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products/create_product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valiator = $request->validate([
            'name' => 'required',
        ]);

        $request['added_by'] = Auth::user()->id;
        $product = Product::create($request->all());

        if($request->hasFile('picture') && $request->file('picture')->isValid()){
            $product->addMediaFromRequest('picture')->toMediaCollection('products_images');
        }

        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        $product_image =$product->getFirstMediaURL('products_images');
        return view('products.product_details', compact('product','product_image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('products.edit_product',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valiator = $request->validate([
            'name' => 'required',
            'unit_selling_price' => 'required',
        ]);
        $product = Product::findOrFail($id);
        $product ->update($request->all());

        if($request->hasFile('picture') && $request->file('picture')->isValid()){
            //clear existing media
            $product->clearMediaCollection('products_images');
            $product->addMediaFromRequest('picture')->toMediaCollection('products_images');
        }

        return redirect('/products/'.$product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        //check if there is an upload and delete it
        if ($product->getFirstMediaURL('products_images') !='') {
            $product->clearMediaCollection('products_images');
        }
        $product->delete();
        return redirect('/products');
    }

    public function AjaxfetchProductImageUrl($product_id)
    {
        $product= Product::findOrFail($product_id);
        $product['image_url'] = $product->getFirstMediaURL('products_images');
        return response()->json($product, 200, []);
    }

}
