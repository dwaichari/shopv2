<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FuelPump;
use App\Models\Product;
use App\Models\FuelPumpReading;
use DB;

class FuelPumpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:can-view-fuel-pumps|can-create-fuel-pumps|can-edit-fuel-pump|can-delete-fuel-pumps', ['only' => ['index','store']]);
         $this->middleware('permission:can-create-fuel-pumps', ['only' => ['create','store']]);
         $this->middleware('permission:can-edit-fuel-pump', ['only' => ['edit','update']]);
         $this->middleware('permission:can-delete-fuel-pumps', ['only' => ['destroy']]);
    }


    public function index()
    {
        $all_pumps = FuelPump::all()->sortByDesc("created_at");
        $pumps = [];
        foreach ($all_pumps as $pump) {
            //fetch previous readings
            $current_reading = DB::table('fuel_pump_readings')->where('pump_id', $pump->id)->latest()->first();
            $pump['current_reading'] = $current_reading;
            array_push($pumps, $pump);
        }
        return view('fuel_pumps.all_pumps', compact('pumps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();

        return view('fuel_pumps.add_fuel_pump', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //save the pump
        FuelPump::create([
            'product_id' =>$request->product_id,
            'pump_name'=>$request->pump_name
        ]);

        return redirect('/fuel_pumps');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function AjaxfetchPumpProduct($pump_id)
    {
        $product = FuelPump::findOrFail($pump_id)->product;
        $product['image_url'] = $product->getFirstMediaURL('products_images');
        return response()->json($product, 200, []);
    }

    public function calibratePump($pump_id)
    {
        $pump = FuelPump::findOrFail($pump_id);
        $pump['image_url'] = $pump->product->getFirstMediaURL('products_images');
        $pump['last_reading'] = DB::table('fuel_pump_readings')->where('pump_id', $pump->id)->latest()->first();

       return view('fuel_pumps.calibrate_pump_readings', compact('pump'));
    }
    public function calibratePumpUpdate(Request $request,$pump_id)
    {
        $pump = FuelPump::findOrFail($pump_id);
        $last_reading = DB::table('fuel_pump_readings')->where('pump_id', $pump->id)->latest()->first();
        $reading  = FuelPumpReading::find($last_reading->id);
        $reading->current_readings = $request->calibration_readings;
        $reading->save();
        
        return redirect('/fuel_pumps');
    }

}
