<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FuelPumpReading extends Model
{
    use HasFactory;

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
    public function pump()
    {
        return $this->belongsTo(FuelPump::class,'pump_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'added_by');
    }
}
