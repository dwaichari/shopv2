<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements HasMedia
{
    use HasFactory,InteractsWithMedia;

    protected $fillable = [
        'name',
        'description',
        'unit_selling_price',
        'in_stock',
        'added_by'
    ];

    public function stocks()
    {
        return $this->hasMany(Stock::class,'product_id');
    }
    public function itemsales()
    {
        return $this->hasMany(ItemSale::class,'product_id');
    }

    //for fuel pumps
    public function fuelpumps()
    {
        return $this->hasMany(FuelPump::class,'product_id');
    }
    //for fuel pumps
    public function fuelpumpsreadings()
    {
        return $this->hasMany(FuelPumpReading::class,'product_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'added_by');
    }
}
