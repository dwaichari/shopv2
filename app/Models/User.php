<?php
  
namespace App\Models;
  
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
  
class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
  
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'added_by');
    }
    public function customers()
    {
        return $this->hasMany(Customer::class, 'added_by');
    }
    public function debts()
    {
        return $this->hasMany(Debt::class, 'added_by');
    }
    public function stocks()
    {
        return $this->hasMany(Stock::class, 'added_by');
    }
    public function sales()
    {
        return $this->hasMany(Sale::class, 'added_by');
    }
    public function fuelpumps()
    {
        return $this->hasMany(FuelPump::class, 'added_by');
    }
    public function fuelpumpreading()
    {
        return $this->hasMany(FuelPumpReading::class, 'added_by');
    }
    public function debt_payments()
    {
        return $this->hasMany(DebtPayment::class, 'added_by');
    }
}