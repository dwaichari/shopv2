<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FuelPump extends Model
{
    use HasFactory;

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    protected $fillable = [
        'product_id',
        'pump_name'
    ];

    public function fuelpumpsreadings()
    {
        return $this->hasMany(FuelPumpReading::class,'product_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'added_by');
    }
}
