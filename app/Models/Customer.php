<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'description','added_by','debt_amount','phone_number'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'added_by');
    }
    public function debts()
    {
        return $this->hasMany(Debt::class, 'customer_id');
    }
    public function debt_payments()
    {
        return $this->hasMany(DebtPayment::class, 'customer_id');
    }
}
