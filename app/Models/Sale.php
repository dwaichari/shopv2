<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable = [
        'added_by',
        'customer_name',
        'total_buying_price',
        'total_selling_price',
        'total_profit',
        'cash_received',
        'balance',
        'status'
    ];

    public function itemsales()
    {
        return $this->hasMany(ItemSale::class, 'sale_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'added_by');
    }

}
