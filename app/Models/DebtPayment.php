<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DebtPayment extends Model
{
    use HasFactory;
    protected $fillable = [
        'description','added_by','amount_paid', 'customer_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'added_by');
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
