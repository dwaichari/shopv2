<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\FuelPumpsController;
use App\Http\Controllers\FuelPumpReadingsController;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/product/{id}/image_url',[ProductsController::class, 'AjaxfetchProductImageUrl']);
Route::get('/pump/{id}/product',[FuelPumpsController::class, 'AjaxfetchPumpProduct']);
Route::get('/fuel_pump/{id}/previous_readings', [FuelPumpReadingsController::class, 'ajaxFetchPumpPreviousReadings']);
Route::get('/get_chart_data/{product_id?}', [SalesController::class, 'ajaxGetChartData']);
