<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\StocksController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\FuelPumpsController;
use App\Http\Controllers\FuelPumpReadingsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\DebtsController;
use App\Http\Controllers\DebtPaymentsController;
use App\Http\Controllers\ExpensesController;
use App\Models\Sale;
use App\Models\Stock;
use App\Models\Product;
use App\Models\FuelPump;
use App\Models\User;
use App\Models\FuelPumpReading;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/business_performance', [App\Http\Controllers\HomeController::class, 'businessPerfomance']);
Route::get('/products_performance', [App\Http\Controllers\HomeController::class, 'productsPerfomance']);
Route::resource('products', ProductsController::class)->middleware('auth');
Route::resource('customers', CustomersController::class)->middleware('auth');
Route::resource('debts', DebtsController::class)->middleware('auth');
Route::resource('stocks', StocksController::class)->middleware('auth');
Route::resource('sales', SalesController::class)->middleware('auth');
Route::resource('fuel_pumps', FuelPumpsController::class)->middleware('auth');
Route::get('fuel_pump/{pump_id}/calibrate', [App\Http\Controllers\FuelPumpsController::class, 'calibratePump'])->middleware('auth');
Route::post('fuel_pump/{pump_id}/calibrate', [App\Http\Controllers\FuelPumpsController::class, 'calibratePumpUpdate'])->middleware('auth');
Route::resource('fuel_pumps_readings', FuelPumpReadingsController::class)->middleware('auth');
Route::get('fuel_pumps_reading/{reading_id}/delete', [App\Http\Controllers\FuelPumpReadingsController::class, 'destroy'])->middleware('auth');
Route::post('/sales_checkout', [SalesController::class, 'checkout'])->middleware('auth');
Route::get('/sales_checkout/{id}', [SalesController::class, 'checkOutForm'])->middleware('auth');
Route::post('/sales_confirm', [SalesController::class, 'confirmSale'])->middleware('auth');
Route::get('/sale_item/remove/{id}', [SalesController::class, 'removeSalesItem'])->middleware('auth');
Route::get('/delete_sales/{id}', [SalesController::class, 'destroy'])->middleware('auth');
Route::get('/unauthorized', [HomeController::class, 'showNoPermissionPage'])->middleware('auth');
Route::resource('roles', RoleController::class)->middleware('auth');
Route::resource('users', UserController::class)->middleware('auth');
Route::resource('debt_payments', DebtPaymentsController::class)->middleware('auth');
Route::resource('expenses', ExpensesController::class)->middleware('auth');
Route::get('/debts_by_customers', [App\Http\Controllers\DebtsController::class, 'debtsByCustomers'])->middleware('auth');



